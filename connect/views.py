from django.shortcuts import render
from rest_framework.decorators import api_view
from django.core.mail import EmailMessage
from django.conf import settings
from rest_framework.response import Response
from rest_framework import status
import requests

# Create your views here.
@api_view(['POST'])
def sendEmail(request):
    to_email = request.data['to_email']
    subject = request.data['subject']
    body = request.data['body']
    attachment_file = request.data['attachment_file']
    email = EmailMessage(
        subject,
        body,
        settings.EMAIL_HOST_USER,
        [to_email]

    )
    email.fail_silently= False
    response = requests.get(attachment_file)
    email.attach('my file', response.content, mimetype='application/pdf')
    email.send()
    return Response('Email has been sent successfully', status=status.HTTP_201_CREATED)